Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly


' TODO: Review the values of the assembly attributes


<Assembly: AssemblyTitle("Activelock COM DLL")>
<Assembly: AssemblyDescription("Activelock Classic - VB2005 version")> 
<Assembly: AssemblyCompany("The ActiveLock Software Group")>
<Assembly: AssemblyProduct("ActiveLock")>
<Assembly: AssemblyCopyright("Copyright 2003-2007")> 
<Assembly: AssemblyTrademark("")>
<Assembly: AssemblyCulture("")>

' Version information for an assembly consists of the following four values:

'	Major version
'	Minor Version
'	Revision
'	Build Number

' You can specify all the values or you can default the Revision and Build Numbers
' by using the '*' as shown below


<Assembly: AssemblyVersion("3.5.5.*")> 
