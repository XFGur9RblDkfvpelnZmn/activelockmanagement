﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
namespace ActiveLockManagement
{
    public partial class Overview : System.Web.UI.Page
    {
        
        public static List<string> appNames = new List<string>();
        

        protected void Page_Load(object sender, EventArgs e)
        {
            appNames.Clear();
            appNames.AddRange(new ActiveLock3_5(null, null, null).GiveAppNames());
            appNames.AddRange(new ActiveLock3_6(null, null, null).GiveAppNames());
            txtApplicationName_SelectedIndexChanged(GridView1.FooterRow.FindControl("txtApplicationName"), null);
            //GridView1.FindControl("ddlAppName").DataBind();

        }
        protected void GridView1_RowCommand1(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Insert"))
            {
                DropDownList u = GridView1.FooterRow.FindControl("txtUserName") as DropDownList;
                DropDownList a = GridView1.FooterRow.FindControl("txtApplicationName") as DropDownList;
                DropDownList l = GridView1.FooterRow.FindControl("txtLicenseType") as DropDownList;
                TextBox m = GridView1.FooterRow.FindControl("txtMaxLicenses") as TextBox;
                TextBox dt = GridView1.FooterRow.FindControl("txtDate") as TextBox;
                TextBox days = GridView1.FooterRow.FindControl("txtDays") as TextBox;
                TextBox regiLevel = GridView1.FooterRow.FindControl("txtRegisteredLevel") as TextBox;
                DropDownList alVersion = GridView1.FooterRow.FindControl("ddlActiveLockVersionFooter") as DropDownList;

                using (SqlConnection con = new SqlConnection(Default.ConnectionString))
                {
                    con.Open();
                    SqlCommand sc = new SqlCommand(@"INSERT into UserApplication
                                                 (UserName,ApplicationName,LicenseType,MaxLicenses,TimeLockedTo,PeriodDays,RegisteredLevel,ActiveLockVersion)
                                                 VALUES
                                                 (@UserName,@ApplicationName,@LicenseType,@MaxLicenses,@TimeLockedTo,@PeriodDays,@RegisteredLevel,@ActiveLockVersion)", con);
                    SqlParameter sp = new SqlParameter("@UserName", u.SelectedValue);
                    sc.Parameters.Add(sp);
                    sp = new SqlParameter("@ApplicationName", a.SelectedValue);
                    sc.Parameters.Add(sp);

                    sp = new SqlParameter("@LicenseType", l.SelectedValue);
                    sc.Parameters.Add(sp);
                    sp = new SqlParameter("@MaxLicenses", m.Text);
                    sc.Parameters.Add(sp);
                    if (l.SelectedValue == "Permanent")
                        dt.Text = "2999/12/31 23:59:59";
                    sp = new SqlParameter("@TimeLockedTo", dt.Text);
                    sc.Parameters.Add(sp);
                    Int32 itmp = 0;
                    Int32.TryParse(days.Text,out itmp);
                    sp = new SqlParameter("@PeriodDays", itmp);
                    sc.Parameters.Add(sp);
                    sp = new SqlParameter("@RegisteredLevel", regiLevel.Text);
                    sc.Parameters.Add(sp);
                    sp = new SqlParameter("@ActiveLockVersion", alVersion.SelectedValue);
                    sc.Parameters.Add(sp);
                    sc.ExecuteNonQuery();
                }
                GridView1.DataBind();

            }
        }
        protected void ddlAppName_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlAppName = (DropDownList)sender;
            GridViewRow row = (GridViewRow)ddlAppName.NamingContainer;
            DropDownList ddlActiveLockVersion = (DropDownList)row.FindControl("ddlActiveLockVersion");
            ddlActiveLockVersion.Items.Clear();
            if (new ActiveLock3_5(null, null, null).GiveAppNames().Contains(ddlAppName.Text))
                ddlActiveLockVersion.Items.Add("ActiveLock3_5NET");
            if (new ActiveLock3_6(null, null, null).GiveAppNames().Contains(ddlAppName.Text))
                ddlActiveLockVersion.Items.Add("ActiveLock3_6NET");
        }

        protected void txtApplicationName_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlAppName = (DropDownList)sender;
            GridViewRow row = (GridViewRow)ddlAppName.NamingContainer;
            DropDownList ddlActiveLockVersion = (DropDownList)row.FindControl("ddlActiveLockVersionFooter");
            ddlActiveLockVersion.Items.Clear();
            if (new ActiveLock3_5(null, null, null).GiveAppNames().Contains(ddlAppName.Text))
                ddlActiveLockVersion.Items.Add("ActiveLock3_5NET");
            if (new ActiveLock3_6(null, null, null).GiveAppNames().Contains(ddlAppName.Text))
                ddlActiveLockVersion.Items.Add("ActiveLock3_6NET");
        }
    }
}