﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MyLicenseKeys.aspx.cs" Inherits="ActiveLockManagement.MyLicenseKeys" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!--<asp:Button ID="btnExport" Text="Export to CSV" runat="server" 
        onclick="btnExport_Click" />-->
    <asp:GridView ID="GridView1" runat="server" 
        AllowSorting="True" AutoGenerateColumns="False" DataSourceID="Default" 
        CellPadding="4" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="UserName" HeaderText="UserName" 
                SortExpression="UserName" />
            <asp:BoundField DataField="ApplicationName" HeaderText="ApplicationName" 
                SortExpression="ApplicationName" />
            <asp:BoundField DataField="LicenseType" HeaderText="LicenseType" 
                SortExpression="LicenseType" />
            <asp:TemplateField Visible="true">
                    <ItemTemplate>
                        <asp:TextBox ID="txtClientKey" runat="server" Visible="false" Text='<%# Bind("ClientKey") %>'></asp:TextBox>
                    </ItemTemplate>
                    
                    <ItemStyle Wrap="False" />
                    
                </asp:TemplateField>
            <asp:BoundField DataField="CreationDate" HeaderText="Creation Date" 
                SortExpression="CreationDate" Visible="true"/>
            <asp:TemplateField HeaderText="Decrypted Client Key">
                <ItemTemplate>
                    <asp:TextBox runat="server" id="lblDecryptedKey" Width="100" Wrap="true" TextMode="MultiLine" Font-Overline="True" Rows="8"></asp:TextBox>
                </ItemTemplate>
                
            </asp:TemplateField>
            <asp:TemplateField HeaderText="License Date" ItemStyle-Wrap="false">
                <ItemTemplate>
                    <asp:Label runat="server" id="lblDecryptedUnlockKey" ></asp:Label>
                </ItemTemplate>
                
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Note" SortExpression="Note">
                <ItemTemplate>
                    <asp:Label ID="lblNote" runat="server" Text='<%# Bind("Note") %>' ></asp:Label>
                </ItemTemplate>
                
            </asp:TemplateField>
            <asp:TemplateField HeaderText="UnlockKey" SortExpression="UnlockKey">
                <ItemTemplate>
                    <asp:TextBox ID="Label1" runat="server" Text='<%# Bind("UnlockKey") %>' Width="500" Wrap="true" TextMode="MultiLine" Font-Overline="True" Rows="8"></asp:TextBox>
                </ItemTemplate>
                
            </asp:TemplateField>
            <asp:TemplateField Visible="true">
                    <ItemTemplate>
                        <asp:TextBox ID="txtActiveLockVersion" runat="server" Visible="false" Text='<%# Bind("ActiveLockVersion") %>'></asp:TextBox>
                    </ItemTemplate>
                    
                    <ItemStyle Wrap="False" />
                    
                </asp:TemplateField>
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <EmptyDataTemplate>

                <div>

                No active licenses available. <br />Create a license key <a href="CreateKey.aspx">here</a>.

                </div>

            </EmptyDataTemplate>
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <asp:SqlDataSource ID="Default" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
        
        
        SelectCommand="SELECT [CreatedKeys].[UserName], [CreatedKeys].[ApplicationName], [CreatedKeys].[LicenseType], [CreatedKeys].[ClientKey], [CreatedKeys].[UnlockKey],[CreatedKeys].[CreationDate],[CreatedKeys].[Note],[ActiveLockVersion] FROM [CreatedKeys] LEFT JOIN [UserApplication] ON [UserApplication].id = UserApplicationId WHERE ([CreatedKeys].[UserName] = @UserName OR @UserRole = 'admin') ORDER BY [CreationDate] DESC">
        <SelectParameters>
            <asp:Parameter Name="UserName" Type="String" DefaultValue="Anonymous" />
            <asp:Parameter Name="UserRole" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>
