﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="CreateKey.aspx.cs" Inherits="ActiveLockManagement.CreateKey" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.dynDateTime.min.js" type="text/javascript"></script>
    <script src="Scripts/calendar-en.min.js" type="text/javascript"></script>
    <link href="Styles/calendar-blue.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $(".dateTimePicker").dynDateTime({
                showsTime: true,
                ifFormat: "%Y/%m/%d %H:%M",
                daFormat: "%l;%M %p, %e %m,  %Y",
                align: "BR",
                electric: false,
                singleClick: false,
                displayArea: ".siblings('.dtcDisplayArea')",
                button: ".next()"
            });
        });
    </script>

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        
    </h2>
    
    <div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataSourceID="Default" onrowcommand="GridView1_RowCommand1" 
            CellPadding="4" ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="ApplicationName" HeaderText="ApplicationName" 
                    SortExpression="ApplicationName" />
                <asp:BoundField DataField="LicenseType" HeaderText="LicenseType" 
                    SortExpression="LicenseType" >
                <ItemStyle Wrap="False" />
                </asp:BoundField>
                <asp:BoundField DataField="MaxLicenses" HeaderText="MaxLicenses" 
                    SortExpression="MaxLicenses" >
                <ItemStyle Wrap="False" />
                </asp:BoundField>
                <asp:BoundField DataField="CreatedKeys" HeaderText="CreatedKeys" 
                    ReadOnly="True" SortExpression="CreatedKeys" >
                    
                <ItemStyle Wrap="False" />
                </asp:BoundField>
                    
                <asp:TemplateField HeaderText="Date" Visible="true">
                    <ItemTemplate>
                        <asp:TextBox ID="Label10" runat="server" CssClass="dateTimePicker" dataformatstring="{0:yyyy'/'MM'/'dd HH:mm:ss}" Text='<%# Bind("TimeLockedTo") %>'></asp:TextBox>
                        <asp:ImageButton  id="btnFillDate" type="button" style="border:none; background-color:transparent" runat="server" ImageUrl="calendar_icon.png"></asp:ImageButton >
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Label ID="Label11" runat="server" Text='<%# Bind("TimeLockedTo") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemStyle Wrap="False" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Days" Visible="true">
                    <ItemTemplate>
                        <asp:TextBox ID="Label12" runat="server" Text='<%# Bind("PeriodDays") %>' Width="70"></asp:TextBox>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Label ID="Label13" runat="server" Text='<%# Bind("PeriodDays") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemStyle Wrap="False" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="RegisteredLevel" Visible="true">
                    <ItemTemplate>
                        <asp:Label ID="Label14" runat="server" Text='<%# Bind("RegisteredLevel") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Label ID="Label15" runat="server" Text='<%# Bind("RegisteredLevel") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemStyle Wrap="True" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Client Key">
                    <ItemTemplate>
                        <asp:TextBox ID="txtClientKey" runat="server" />
                    </ItemTemplate>
                    <ItemStyle Wrap="False" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Note">
                    <ItemTemplate>
                        <asp:TextBox ID="txtNote" runat="server" Width="80" />
                    </ItemTemplate>
                    <ItemStyle Wrap="False" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Create Key">
                    <ItemTemplate>
                        <asp:Button ID="Button1" runat="server" 
                            CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" 
                            CommandName="CreateKey" Enabled="true" Text="Create Key" />
                    </ItemTemplate>
                    <ItemStyle Wrap="False" />
                </asp:TemplateField>
                <asp:TemplateField Visible="true">
                    <ItemTemplate>
                        <asp:TextBox ID="Label15" runat="server" Visible="false" CssClass="dateTimePicker" Text='<%# Bind("TimeLockedTo") %>'></asp:TextBox>
                    </ItemTemplate>
                    
                    <ItemStyle Wrap="False" />
                    
                </asp:TemplateField>
                <asp:TemplateField Visible="true">
                    <ItemTemplate>
                        <asp:TextBox ID="Label16" runat="server" Visible="false" Text='<%# Bind("PeriodDays") %>'></asp:TextBox>
                    </ItemTemplate>
                    
                    <ItemStyle Wrap="False" />
                    
                </asp:TemplateField>
                <asp:TemplateField Visible="true">
                    <ItemTemplate>
                        <asp:TextBox ID="lblId" runat="server" Visible="false" Text='<%# Bind("id") %>'></asp:TextBox>
                    </ItemTemplate>
                    
                    <ItemStyle Wrap="False" />
                    
                </asp:TemplateField>
                <asp:TemplateField Visible="true">
                    <ItemTemplate>
                        <asp:TextBox ID="lblActiveLockVersion" runat="server" Visible="false" Text='<%# Bind("ActiveLockVersion") %>'></asp:TextBox>
                    </ItemTemplate>
                    
                    <ItemStyle Wrap="False" />
                    
                </asp:TemplateField>
                
                
                
                
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <EmptyDataTemplate>

                <div>

                No active licenses available. <br />Contact ReBuS.

                </div>

            </EmptyDataTemplate>
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
    
        <asp:SqlDataSource ID="Default" runat="server" 
            ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
            SelectCommand="SELECT UserApplication.id,UserApplication.ApplicationName,  UserApplication.LicenseType, UserApplication.MaxLicenses, COUNT(CreatedKeys.ClientKey) AS CreatedKeys, TimeLockedTo, PeriodDays,RegisteredLevel,ActiveLockVersion FROM UserApplication LEFT OUTER JOIN CreatedKeys ON UserApplication.UserName = CreatedKeys.UserName AND UserApplication.ApplicationName = CreatedKeys.ApplicationName AND UserApplication.LicenseType = CreatedKeys.LicenseType AND UserApplication.id = CreatedKeys.UserApplicationId WHERE UserApplication.UserName = @UserName   GROUP BY UserApplication.ApplicationName, UserApplication.LicenseType, UserApplication.MaxLicenses,TimeLockedTo, PeriodDays,RegisteredLevel,UserApplication.id,ActiveLockVersion">
              <SelectParameters>
                <asp:Parameter Name="UserName" Type="String" DefaultValue="0" />
              </SelectParameters>
        </asp:SqlDataSource>
    
    </div>
    <br />
    <div id="KeyField" runat="server" style="text-align:justify;">
        <div id="KeyFieldLabel" runat="server"></div>
        <asp:TextBox runat="server" ID="KeyFieldText" rows="8" Columns="70" TextMode="multiline"></asp:TextBox>
    </div>
    
</asp:Content>

    