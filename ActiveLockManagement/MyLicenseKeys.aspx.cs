﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using ActiveLock3_5NET;
using System.Text.RegularExpressions;
namespace ActiveLockManagement
{
    public partial class MyLicenseKeys : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Default.SelectParameters["UserName"].DefaultValue = this.Page.User.Identity.Name;
            Default.SelectParameters["UserRole"].DefaultValue = this.Page.User.IsInRole("admin") ? "admin" : "client";
            
            foreach (GridViewRow row in GridView1.Rows)
            {
                string decoded = string.Empty;
                string decoded2 = string.Empty;
                switch(((TextBox)row.Cells[9].FindControl("txtActiveLockVersion")).Text)
                {
                    case "ActiveLock3_5NET":
                        ActiveLock3_5 al = new ActiveLock3_5("", "", "");
                        decoded = al.base64Decode(((TextBox)row.Cells[3].FindControl("txtClientKey")).Text);
                        decoded2 = al.base64Decode(((TextBox)row.Cells[7].FindControl("Label1")).Text);
                        break;
                    case "ActiveLock3_6NET":
                        ActiveLock3_6 al2 = new ActiveLock3_6("", "", "");
                        decoded = al2.base64Decode(((TextBox)row.Cells[3].FindControl("txtClientKey")).Text);
                        decoded2 = al2.base64Decode(((TextBox)row.Cells[7].FindControl("Label1")).Text);
                        
                        break;
                    default:
                        throw new NotImplementedException();
                        break;
                }
                DateTime dtEnd = DateTime.MinValue;
                ((TextBox)row.Cells[6].FindControl("lblDecryptedKey")).Text = decoded;
                foreach (string lijn in System.Text.RegularExpressions.Regex.Split(decoded2, Environment.NewLine))
                {
                    DateTime dt = DateTime.MinValue;
                    DateTime.TryParse(lijn.Trim(), out dt);
                    if (dt > DateTime.MinValue)
                        dtEnd = dt;
                }
               
                if (row.Cells[2].Text.Trim() != "Permanent")
                    ((Label)row.Cells[6].FindControl("lblDecryptedUnlockKey")).Text = dtEnd.ToString("yyyy'/'MM'/'dd HH:ss");
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
             "attachment;filename=Export.csv");
            Response.Charset = "";
            Response.ContentType = "application/text";

            GridView1.AllowPaging = false;
            GridView1.DataBind();

            StringBuilder sb = new StringBuilder();
            for (int k = 0; k < GridView1.Columns.Count; k++)
            {
                //add separator
                sb.Append(GridView1.Columns[k].HeaderText + ',');
            }
            //append new line
            sb.Append("\r\n");
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                for (int k = 0; k < GridView1.Columns.Count; k++)
                {
                    
                    //add separator
                    sb.Append(GridView1.Rows[i].Cells[k].Text.Replace('\r', ' ').Replace('\n', ' ') + ',');
                }
                //append new line
                sb.Append("\r\n");
            }
            Response.Output.Write(sb.ToString());
            Response.Flush();
            Response.End();
        }

    }
}