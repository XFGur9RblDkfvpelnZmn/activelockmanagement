﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;


namespace ActiveLockManagement
{
    public enum LicenseType
    {
        Permanent, TimeLocked, Periodic
    
    }
   
    public abstract class ActiveLock
    {

        abstract public string mProductsStoragePath();


        abstract public string CreateKey(LicenseType lt, DateTime dt, int iDays, string registeredLevel);
        abstract public string base64Decode(string sEncryptedString);

        public static string Make64ByteChunks(string strdata)
        {
            // Breaks a long string into chunks of 64-byte lines.
            Int32 i;
            Int32 Count;
            string strNew64Chunk = string.Empty;
            string sResult = string.Empty;


            Count = strdata.Length;
            for (i = 0; i <= Count; i += 64)
            {

                if (i + 64 > Count)
                {
                    strNew64Chunk = strdata.Substring(i);
                }
                else
                {
                    strNew64Chunk = strdata.Substring(i, 64);
                }
                if (sResult.Length > 0)
                {
                    //+ Environment.NewLine
                    sResult = sResult + strNew64Chunk;
                }
                else
                {
                    sResult = sResult + strNew64Chunk;
                }
            }

            return sResult;
        }
               
        public static string rsaDecrypt(string sEncryptedString)
        {
            try
            {
                return System.Text.Encoding.UTF8.GetString(ActiveLock3_5NET.Crypto.RSADecrypt(sEncryptedString));
                
            }
            catch
            {
                return "Error decrypting string";
            }

        }

        abstract public List<string> GiveAppNames();
    }

    public class ActiveLock3_6 : ActiveLock
    {

        public override string mProductsStoragePath()
        {
            return Properties.Settings.Default.LocalPath + "\\licenses36.ini";
        }
        #region stdparams
        private static ActiveLock3_6NET.Globals activeLock3Globals_definst = new ActiveLock3_6NET.Globals();



        ActiveLock3_6NET.AlugenGlobals MyGen = new ActiveLock3_6NET.AlugenGlobals();
        ActiveLock3_6NET._IActiveLock Instance;
        ActiveLock3_6NET.IActiveLock.LicStoreType mKeyStoreType = ActiveLock3_6NET.IActiveLock.LicStoreType.alsFile;
        ActiveLock3_6NET.IActiveLock.ProductsStoreType mProductsStoreType = ActiveLock3_6NET.IActiveLock.ProductsStoreType.alsINIFile;
        

        ActiveLock3_6NET._IALUGenerator GeneratorInstance;
        #endregion
        #region variable params
        string appName;
        string version;
        
        /// <summary>
        /// Networked or single license (default single)
        /// </summary>
        ActiveLock3_6NET.ProductLicense.LicFlags licFlags = ActiveLock3_6NET.ProductLicense.LicFlags.alfSingle;
        /// <summary>
        /// permanent, timelocked, periodic (default permanent)
        /// </summary>
        ActiveLock3_6NET.ProductLicense.ALLicType allicType = ActiveLock3_6NET.ProductLicense.ALLicType.allicPermanent;
        /// <summary>
        /// Use this var if you want to make use of different registration levels
        /// </summary>
        string registeredLevel = "1";
        /// <summary>
        /// experationDate (default 1 year)
        /// </summary>
        DateTime expirationDate = DateTime.Now.AddYears(1);
        /// <summary>
        /// creation date (default now)
        /// </summary>
        DateTime creationDate = DateTime.Now;
        /// <summary>
        /// Max number of users (default 1)
        /// </summary>
        short maxUsers = 1;
        string strInstallKey = string.Empty;

        #endregion
        #region working vars

        ActiveLock3_6NET.ProductLicense Lic;
        string strLibKey = string.Empty;
        string strChunks = string.Empty;

        #endregion

        public ActiveLock3_6(string appName, string version, string installKey)
        {
            Instance = activeLock3Globals_definst.NewInstance();
            Instance.KeyStoreType = mKeyStoreType;
            string autoLicString = string.Empty;
            Instance.LockType = ActiveLock3_6NET.IActiveLock.ALLockTypes.lockHDFirmware;
            
            string s = Environment.CurrentDirectory;
            Instance.Init(Properties.Settings.Default.LocalPath, ref autoLicString);
            GeneratorInstance = MyGen.GeneratorInstance(mProductsStoreType);
            GeneratorInstance.StoragePath = mProductsStoragePath();
            this.appName = appName;
            this.version = version;
            this.strInstallKey = installKey;

        }

        public override string CreateKey(LicenseType lt, DateTime dt, int iDays, string registeredLevel)
        {
            switch (lt)
            {
                case LicenseType.Periodic:
                    allicType = ActiveLock3_6NET.ProductLicense.ALLicType.allicPeriodic;
                    if (dt < new DateTime(2000,1,1))
                        dt = DateTime.Today.AddDays(iDays).AddHours(13);
                    break;
                case LicenseType.Permanent:
                    allicType = ActiveLock3_6NET.ProductLicense.ALLicType.allicPermanent;
                    break;
                case LicenseType.TimeLocked:
                    allicType = ActiveLock3_6NET.ProductLicense.ALLicType.allicTimeLocked;
                    if (dt < new DateTime(2000, 1, 1) || DateTime.Today.AddDays(iDays).AddHours(13) < dt)
                        dt = DateTime.Today.AddDays(iDays).AddHours(13);
                    break;
            }
            if (dt != null)
                expirationDate = dt;
            Lic = activeLock3Globals_definst.CreateProductLicense(appName, version, " ", licFlags, allicType, " ", registeredLevel, expirationDate, null, creationDate, null, maxUsers);
            ActiveLock3_6NET._IActiveLock al = new ActiveLock3_6NET.IActiveLock();
            
            string strLibKey = GeneratorInstance.GenKey(ref Lic, strInstallKey, registeredLevel);
            string strChunks = Make64ByteChunks(strLibKey + "aLck" + strInstallKey);
            return strChunks;
        }


        public override string base64Decode(string sEncryptedString)
        {
            try
            {
                return activeLock3Globals_definst.Base64Decode(sEncryptedString);
                
            }
            catch
            {
                return "Error decrypting string";
            }
        }
        public override List<string> GiveAppNames()
        {
            List<string> appNames = new List<string>();
            IniFileName i = new IniFileName(mProductsStoragePath());
            string[] names = i.GetSectionNames();
            foreach (string s in names)
            {
                appNames.Add(s);
            }
            return appNames;
        }
        

    }
    public class ActiveLock3_5 : ActiveLock
    {
        public override string mProductsStoragePath()
        {
            return Properties.Settings.Default.LocalPath + "\\licenses35.ini";
        }
       #region stdparams
        private static ActiveLock3_5NET.Globals activeLock3Globals_definst = new ActiveLock3_5NET.Globals();



        ActiveLock3_5NET.AlugenGlobals MyGen = new ActiveLock3_5NET.AlugenGlobals();
        ActiveLock3_5NET._IActiveLock Instance;
        ActiveLock3_5NET.IActiveLock.LicStoreType mKeyStoreType = ActiveLock3_5NET.IActiveLock.LicStoreType.alsFile;
        ActiveLock3_5NET.IActiveLock.ProductsStoreType mProductsStoreType = ActiveLock3_5NET.IActiveLock.ProductsStoreType.alsINIFile;

        ActiveLock3_5NET._IALUGenerator GeneratorInstance;
        #endregion
        #region variable params
        string appName;
        string version;
        
        /// <summary>
        /// Networked or single license (default single)
        /// </summary>
        ActiveLock3_5NET.ProductLicense.LicFlags licFlags = ActiveLock3_5NET.ProductLicense.LicFlags.alfSingle;
        /// <summary>
        /// permanent, timelocked, periodic (default permanent)
        /// </summary>
        ActiveLock3_5NET.ProductLicense.ALLicType allicType = ActiveLock3_5NET.ProductLicense.ALLicType.allicPermanent;
        /// <summary>
        /// Use this var if you want to make use of different registration levels
        /// </summary>
        string registeredLevel = "1";
        /// <summary>
        /// experationDate (default 1 year)
        /// </summary>
        DateTime expirationDate = DateTime.Now.AddYears(1);
        /// <summary>
        /// creation date (default now)
        /// </summary>
        DateTime creationDate = DateTime.Now;
        /// <summary>
        /// Max number of users (default 1)
        /// </summary>
        short maxUsers = 1;
        string strInstallKey = string.Empty;

        #endregion
        #region working vars

        ActiveLock3_5NET.ProductLicense Lic;
        string strLibKey = string.Empty;
        string strChunks = string.Empty;

        #endregion

        public ActiveLock3_5(string appName, string version, string installKey)
        {
            Instance = activeLock3Globals_definst.NewInstance();
            Instance.KeyStoreType = mKeyStoreType;
            string autoLicString = string.Empty;
            Instance.LockType = ActiveLock3_5NET.IActiveLock.ALLockTypes.lockHDFirmware;
            
            string s = Environment.CurrentDirectory;
            Instance.Init(Properties.Settings.Default.LocalPath, ref autoLicString);
            GeneratorInstance = MyGen.GeneratorInstance(mProductsStoreType);
            GeneratorInstance.StoragePath = mProductsStoragePath();
            this.appName = appName;
            this.version = version;
            this.strInstallKey = installKey;

        }

        public override string CreateKey(LicenseType lt, DateTime dt, int iDays, string registeredLevel)
        {
            switch (lt)
            {
                case LicenseType.Periodic:
                    allicType = ActiveLock3_5NET.ProductLicense.ALLicType.allicPeriodic;
                    if (dt < new DateTime(2000,1,1))
                        dt = DateTime.Today.AddDays(iDays).AddHours(13);
                    break;
                case LicenseType.Permanent:
                    allicType = ActiveLock3_5NET.ProductLicense.ALLicType.allicPermanent;
                    break;
                case LicenseType.TimeLocked:
                    allicType = ActiveLock3_5NET.ProductLicense.ALLicType.allicTimeLocked;
                    if (dt < new DateTime(2000, 1, 1))
                        dt = DateTime.Today.AddDays(iDays).AddHours(13);
                    break;
            }
            if (dt != null)
                expirationDate = dt;
            Lic = activeLock3Globals_definst.CreateProductLicense(appName, version, "", licFlags, allicType, "", registeredLevel, expirationDate.ToString("yyyy'/'MM'/'dd HH:mm:ss"), null, creationDate.ToString("yyyy'/'MM'/'dd"), null, maxUsers);
            
            string strLibKey = GeneratorInstance.GenKey(ref Lic, strInstallKey, registeredLevel);
            string strChunks = Make64ByteChunks(strLibKey + "aLck" + strInstallKey);
            return strChunks;
        }


        public override string base64Decode(string sEncryptedString)
        {
            try
            {
                return activeLock3Globals_definst.Base64Decode(sEncryptedString);
                
            }
            catch
            {
                return "Error decrypting string";
            }
        }
        public override List<string> GiveAppNames()
        {
            List<string> appNames = new List<string>();
            IniFileName i = new IniFileName(mProductsStoragePath());
            string[] names = i.GetSectionNames();
            foreach (string s in names)
            {
                appNames.Add(s);
            }
            return appNames;
        }
    }
}
