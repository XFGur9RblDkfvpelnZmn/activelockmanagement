﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Net.Mail;
using System.Net;
using System.Data.SqlClient;
using System.Globalization;

namespace ActiveLockManagement
{
    public partial class CreateKey : System.Web.UI.Page
    {
        string UserName = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            Default.SelectParameters["UserName"].DefaultValue = HttpContext.Current.User.Identity.Name;
            UserName = HttpContext.Current.User.Identity.Name;

            foreach (GridViewRow row in GridView1.Rows)
            {
                switch (((TableCell)row.Cells[1]).Text)
                {
                    case "Permanent":
                        ((TextBox)row.Cells[4].FindControl("Label10")).Text = "";
                        ((TextBox)row.Cells[5].FindControl("Label12")).Text = "";
                        ((TextBox)row.Cells[4].FindControl("Label10")).Visible = false;
                        ((ImageButton)row.Cells[4].FindControl("btnFillDate")).Visible = false;
                        ((TextBox)row.Cells[5].FindControl("Label12")).Visible = false;
                        break;
                    case "TimeLocked":
                        //((TextBox)row.Cells[5].FindControl("Label12")).Text = "";
                        ((TextBox)row.Cells[5].FindControl("Label12")).Visible = false;
                        DateTime dt = DateTime.Now.AddDays(-1);
                        int iDays = 0;
                        DateTime.TryParse(((TextBox)row.Cells[4].FindControl("Label10")).Text, out dt);
                        Int32.TryParse(((TextBox)row.Cells[5].FindControl("Label12")).Text, out iDays);
                        if (dt > DateTime.Today.AddHours(13).AddDays(iDays) && !IsPostBack)
                        {
                            dt = DateTime.Today.AddHours(13).AddDays(iDays);
                            ((TextBox)row.Cells[4].FindControl("Label10")).Text = dt.ToString("yyyy/MM/dd HH:mm:ss");
                        }
                        break;
                    case "Periodic":
                        //((TextBox)row.Cells[4].FindControl("Label10")).Text = "";
                        ((ImageButton)row.Cells[4].FindControl("btnFillDate")).Visible = false;
                        //((TextBox)row.Cells[4].FindControl("Label10")).Visible = false;
                        ((TextBox)row.Cells[4].FindControl("Label10")).Enabled = false;
                        break;
                }
            }
        }
        


        protected void GridView1_RowCommand1(object sender, GridViewCommandEventArgs e)
        {
            //79.143.222.193
            //eigen username/pass
            
            if (e.CommandName == "CreateKey")
            {
                // Retrieve the row index stored in the 
                // CommandArgument property.
                int index = Convert.ToInt32(e.CommandArgument);

                // Retrieve the row that contains the button 
                // from the Rows collection.
                GridViewRow row = GridView1.Rows[index];
                LicenseType lt = LicenseType.TimeLocked;
                if (((TextBox)row.Cells[7].FindControl("txtClientKey")).Text.Trim().Length > 2)
                {
                    
                    switch (row.Cells[1].Text)
                    {
                        case "Permanent":
                            lt = LicenseType.Permanent;
                            break;
                        case "TimeLocked":
                            lt = LicenseType.TimeLocked;
                            break;
                        case "Periodic":
                            lt = LicenseType.Periodic;
                            break;

                    }
                    // BUGFIX VERSION NR
                    string[] splitData = row.Cells[0].Text.Split(' ');
                    string appName = string.Empty;
                    string version = string.Empty;
                    for (int i = 0; i < splitData.Length - 1; i++)
                    {
                        appName = splitData[i] + " ";
                    }
                    appName = appName.Substring(0, appName.Length - 1);
                    version = splitData[splitData.Length - 1];


                    try
                    {
                        Int32 Max = 0;
                        Int32 Created = 0;
                        Int32.TryParse(row.Cells[2].Text.ToString(), out Max);
                        Int32.TryParse(row.Cells[3].Text.ToString(), out Created);

                        if (Max > Created)
                        {
                            string clientKey = ((TextBox)row.Cells[7].FindControl("txtClientKey")).Text;
                            string registeredLevel = ((Label)row.Cells[6].FindControl("Label14")).Text;
                            string note = ((TextBox)row.Cells[8].FindControl("txtNote")).Text;
                            DateTime dt = DateTime.Now.AddDays(-1);
                            int iDays = 0;
                            //DateTime.TryParseExact(((TextBox)row.Cells[4].FindControl("Label10")).Text, "yyyy'/'MM'/'dd HH:mm:ss",CultureInfo.InvariantCulture, DateTimeStyles.None,  out dt);
                            DateTime.TryParse(((TextBox)row.Cells[4].FindControl("Label10")).Text, out dt);
                            Int32.TryParse(((TextBox)row.Cells[5].FindControl("Label12")).Text, out iDays);

                            DateTime dtMax = DateTime.Now.AddDays(-1);
                            int iDaysMax = 0;
                            DateTime.TryParse(((TextBox)row.Cells[9].FindControl("Label15")).Text, out dtMax);

                            Int32.TryParse(((TextBox)row.Cells[10].FindControl("Label16")).Text, out iDaysMax);
                            string id = ((TextBox)row.Cells[11].FindControl("lblId")).Text;

                            if (dt > dtMax || iDays > iDaysMax || lt == LicenseType.Periodic && iDays < 1 || (lt == LicenseType.TimeLocked && dt > DateTime.Today.AddHours(13).AddDays(iDays)))
                            {
                                //KeyFieldLabel.InnerHtml = "<h2>INCORRECT DATE or DAYS</h2> The default value is a maximum value";
                                if (dt > dtMax)
                                {
                                    KeyFieldLabel.InnerHtml = "<h2>INCORRECT DATE or DAYS</h2> The default value exceeds " + dtMax.ToString("yyyy'/'MM'/'dd HH:mm:ss") + ".";
                                }
                                else if (iDays > iDaysMax)
                                {
                                    KeyFieldLabel.InnerHtml = "<h2>INCORRECT DATE or DAYS</h2> The default value exceeds " + iDaysMax + " days.";
                                }
                                else if (lt == LicenseType.Periodic && iDays < 1)
                                {
                                    KeyFieldLabel.InnerHtml = "<h2>INCORRECT DATE or DAYS</h2> The default value must be greater than zero.";
                                }
                                else if (lt == LicenseType.TimeLocked && dt > DateTime.Today.AddHours(13).AddDays(iDays))
                                {
                                    KeyFieldLabel.InnerHtml = "<h2>INCORRECT DATE or DAYS</h2> The default value exceeds " + DateTime.Today.AddHours(13).AddDays(iDays).ToString("yyyy'/'MM'/'dd HH:mm:ss") + ".";
                                }
                                else
                                {
                                    KeyFieldLabel.InnerHtml = "<h2>INCORRECT DATE or DAYS</h2> The default value exceeds " + dtMax.ToString("yyyy'/'MM'/'dd HH:mm:ss") + " or " + iDaysMax + " days.";
                                }
                                KeyFieldText.Text = string.Empty;
                                ((TextBox)row.Cells[4].FindControl("Label10")).Text = dtMax.ToString("yyyy'/'MM'/'dd HH:mm:ss");

                                ((TextBox)row.Cells[5].FindControl("Label12")).Text = iDaysMax.ToString();
                            }
                            else
                            {
                                string activeLockVersion = ((TextBox)row.Cells[12].FindControl("lblActiveLockVersion")).Text;
                                ActiveLock al;
                                if (activeLockVersion == "ActiveLock3_5NET")
                                    al = new ActiveLock3_5(appName, version, clientKey);
                                else if (activeLockVersion == "ActiveLock3_6NET")
                                    al = new ActiveLock3_6(appName, version, clientKey);
                                else
                                    throw new NotImplementedException();

                                string unlockKey = al.CreateKey(lt, dt, iDays, registeredLevel);

                                using (SqlConnection sc = new SqlConnection(Default.ConnectionString))
                                {
                                    sc.Open();
                                    SqlCommand select = new SqlCommand(@"SELECT COUNT(*) FROM CreatedKeys WHERE UnlockKey = @UnlockKey ", sc);
                                    SqlParameter sp = new SqlParameter("UnlockKey", unlockKey);
                                    select.Parameters.Add(sp);
                                    int count = (int)select.ExecuteScalar();
                                    if (count > 0)
                                    {
                                        GridView1.DataBind();
                                        KeyFieldLabel.InnerHtml = "<h2>Your Key:</h2> <br />(Key already existed, no extra license used)<br />";
                                        KeyFieldText.Text = unlockKey;
                                    }
                                    else
                                    {
                                        SqlCommand com = new SqlCommand(@"INSERT into CreatedKeys
                                                                (UserName,ApplicationName,LicenseType,ClientKey,UnlockKey,Note,UserApplicationId)
                                                                VALUES
                                                                (@UserName,@ApplicationName,@LicenseType,@ClientKey,@UnlockKey,@Note,@UserApplicationId)", sc);
                                        sp = new SqlParameter("UserName", UserName);
                                        com.Parameters.Add(sp);
                                        sp = new SqlParameter("ApplicationName", appName + " " + version);
                                        com.Parameters.Add(sp);
                                        sp = new SqlParameter("LicenseType", lt.ToString());
                                        com.Parameters.Add(sp);
                                        sp = new SqlParameter("ClientKey", clientKey);
                                        com.Parameters.Add(sp);
                                        sp = new SqlParameter("UnlockKey", unlockKey);
                                        com.Parameters.Add(sp);
                                        sp = new SqlParameter("Note", note);
                                        com.Parameters.Add(sp);
                                        sp = new SqlParameter("UserApplicationId", id);
                                        com.Parameters.Add(sp);
                                        com.ExecuteNonQuery();

                                        GridView1.DataBind();
                                        KeyFieldLabel.InnerHtml = "<h2>Your Key:</h2> <br />";
                                        KeyFieldText.Text = unlockKey;
                                        MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                                        if (user != null)
                                        {
                                            //string emailAddress = user.Email;
                                            //System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                                            //message.To.Add(user.Email);
                                            //message.Subject = "License Key";
                                            //message.From = new System.Net.Mail.MailAddress("pieter.den.dooven@rebus-it.be");

                                            //message.Body = unlockKey;
                                            //System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("192.168.50.35");
                                            //NetworkCredential basicCredential = new NetworkCredential(@"pdoo", "Drieendertig33");
                                            //smtp.Credentials = basicCredential;
                                            //smtp.EnableSsl = true;
                                            //smtp.Send(message);
                                            //set your label text here
                                        }
                                    }

                                }
                            }
                            
                        }
                        else
                        {
                            KeyFieldLabel.InnerHtml = "<h2>NO LICENSES LEFT</h2> Contact ReBuS.";
                            KeyFieldText.Text = string.Empty;
                        }
                    }
                    catch (Exception ex)
                    {
                        KeyField.InnerHtml = ex.ToString();
                    }



                }
                else
                {
                    KeyFieldLabel.InnerHtml = "<h2>INCORRECT KEY</h2>";
                    KeyFieldText.Text = string.Empty;
                }
            }
            //reset layout
            Page_Load(null, null);
        }

        
    }
}