﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Overview.aspx.cs" Inherits="ActiveLockManagement.Overview" %>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.dynDateTime.min.js" type="text/javascript"></script>
    <script src="Scripts/calendar-en.min.js" type="text/javascript"></script>
    <link href="Styles/calendar-blue.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $(".dateTimePicker").dynDateTime({
                showsTime: true,
                ifFormat: "%Y/%m/%d %H:%M",
                daFormat: "%l;%M %p, %e %m,  %Y",
                align: "BR",
                electric: false,
                singleClick: false,
                displayArea: ".siblings('.dtcDisplayArea')",
                button: ".next()"
            });
        });
    </script>

</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataSourceID="Default" AllowSorting="True" 
            DataKeyNames="ID" ShowFooter="True" 
            onrowcommand="GridView1_RowCommand1" CellPadding="4" ForeColor="#333333" 
            GridLines="None">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:CommandField ShowEditButton="True" />
                <asp:TemplateField HeaderText="ID" InsertVisible="False" SortExpression="ID">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                    </EditItemTemplate>
                    <FooterTemplate>
                       
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="UserName" SortExpression="UserName">
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("UserName") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList ID="TextBox1" runat="server" Text='<%# Bind("UserName") %>' 
                            DataSourceID="UserName" DataTextField="UserName" DataValueField="UserName"></asp:DropDownList>
                        
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:DropDownList ID="txtUserName" runat="server" Text='<%# Bind("UserName") %>' 
                            DataSourceID="UserName" DataTextField="UserName" DataValueField="UserName"></asp:DropDownList>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ApplicationName" 
                    SortExpression="ApplicationName">
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("ApplicationName") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList ID="ddlAppName" runat="server" DataSource="<%# appNames %>" Text='<%# Bind("ApplicationName") %>' OnSelectedIndexChanged="ddlAppName_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:DropDownList ID="txtApplicationName" runat="server" DataSource="<%# appNames %>" OnSelectedIndexChanged="txtApplicationName_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                    </FooterTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="LicenseType" SortExpression="LicenseType">
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("LicenseType") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList ID="TextBox4" runat="server" Text='<%# Bind("LicenseType") %>'>
                            <asp:ListItem Value="TimeLocked">Time Locked</asp:ListItem>
                            <asp:ListItem>Periodic</asp:ListItem>
                            <asp:ListItem>Permanent</asp:ListItem>
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:DropDownList ID="txtLicenseType" runat="server" Text='<%# Bind("LicenseType") %>'>
                            <asp:ListItem Value="TimeLocked">Time Locked</asp:ListItem>
                            <asp:ListItem>Periodic</asp:ListItem>
                            <asp:ListItem>Permanent</asp:ListItem>
                        </asp:DropDownList>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Date" SortExpression="TimeLockedTo">
                    <ItemTemplate>
                        <asp:Label ID="Label10" runat="server" Text='<%# Bind("TimeLockedTo") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox10" runat="server" CssClass="dateTimePicker" Text='<%# Bind("TimeLockedTo") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtDate" runat="server"  CssClass="dateTimePicker" Text='<%# DateTime.Now.AddYears(1).ToString("yyyy/MM/dd 13:00:00") %>'></asp:TextBox>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Days" SortExpression="PeriodDays">
                    <ItemTemplate>
                        <asp:Label ID="Label11" runat="server" Text='<%# Bind("PeriodDays") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("PeriodDays") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtDays" runat="server" Text='<%# Bind("PeriodDays") %>'></asp:TextBox>
                    </FooterTemplate>
                </asp:TemplateField>
               <asp:TemplateField HeaderText="RegisteredLevel" SortExpression="RegisteredLevel">
                    <ItemTemplate>
                        <asp:Label ID="Label12" runat="server" Text='<%# Bind("RegisteredLevel") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox12" runat="server" Text='<%# Bind("RegisteredLevel") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtRegisteredLevel" runat="server" Text='<%# Bind("RegisteredLevel") %>'></asp:TextBox>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ActiveLockVersion" SortExpression="ActiveLockVersion">
                    <ItemTemplate>
                        <asp:Label ID="lblActiveLockVersion" runat="server" Text='<%# Bind("ActiveLockVersion") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList ID="ddlActiveLockVersion" runat="server" Text='<%# Bind("ActiveLockVersion") %>'>
                            <asp:ListItem>ActiveLock3_5NET</asp:ListItem>
                            <asp:ListItem>ActiveLock3_6NET</asp:ListItem>
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:DropDownList ID="ddlActiveLockVersionFooter" runat="server" Text='<%# Bind("ActiveLockVersion") %>'>
                            <asp:ListItem>ActiveLock3_5NET</asp:ListItem>
                            <asp:ListItem>ActiveLock3_6NET</asp:ListItem>
                        </asp:DropDownList>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="MaxLicenses" SortExpression="MaxLicenses">
                    <ItemTemplate>
                        <asp:Label ID="Label6" runat="server" Text='<%# Bind("MaxLicenses") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("MaxLicenses") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtMaxLicenses" runat="server" Text='<%# Bind("MaxLicenses") %>'></asp:TextBox>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CreatedKeys" SortExpression="CreatedKeys">
                    <ItemTemplate>
                        <asp:Label ID="Label7" runat="server" Text='<%# Bind("CreatedKeys") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("CreatedKeys") %>'></asp:Label>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:Button ID="btnInsert" runat="Server" Text="Insert" CommandName="Insert" UseSubmitBehavior="False" />
                    </FooterTemplate>
                </asp:TemplateField>
                
                
                
                
                
                
                
                 
                
                
                
                
                
                
                
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
    
        <asp:SqlDataSource ID="Default" runat="server" 
            ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
            SelectCommand="SELECT UserApplication.ID, UserApplication.UserName, UserApplication.ApplicationName, UserApplication.LicenseType, UserApplication.MaxLicenses, COUNT(CreatedKeys.ClientKey) AS CreatedKeys, TimeLockedTo, PeriodDays,RegisteredLevel, ActiveLockVersion FROM UserApplication LEFT OUTER JOIN CreatedKeys ON UserApplication.UserName = CreatedKeys.UserName AND UserApplication.ApplicationName = CreatedKeys.ApplicationName  AND UserApplication.LicenseType = CreatedKeys.LicenseType AND UserApplication.id = CreatedKeys.UserApplicationId GROUP BY UserApplication.ApplicationName, UserApplication.LicenseType, UserApplication.MaxLicenses, UserApplication.UserName,UserApplication.ID, UserApplication.TimeLockedTo, UserApplication.PeriodDays,RegisteredLevel,ActiveLockVersion"
            UpdateCommand="UPDATE UserApplication SET UserName = @UserName,ApplicationName = @ApplicationName, LicenseType = @LicenseType, MaxLicenses = @MaxLicenses, TimeLockedTo = @TimeLockedTo,PeriodDays = @PeriodDays,RegisteredLevel = @RegisteredLevel, ActiveLockVersion = @ActiveLockVersion WHERE UserApplication.ID = @ID"
            InsertCommand="INSERT INTO [UserApplication] ([UserName],[ApplicationName],[LicenseType], [MaxLicenses], [TimeLockedTo], [PeriodDays],[RegisteredLevel], [ActiveLockVersion]) VALUES (@UserName, @ApplicationName,@LicenseType,@MaxLicenses,@TimeLockedTo,@PeriodDays,@RegisteredLevel, @ActiveLockVersion)">
            <InsertParameters>
                <asp:Parameter Name="UserName" />
                <asp:Parameter Name="ApplicationName" />
                
                <asp:Parameter Name="LicenseType" />
                <asp:Parameter Name="MaxLicenses" />
                <asp:Parameter 
                  Name="TimeLockedTo"></asp:Parameter>
                <asp:Parameter
                  Name="PeriodDays"></asp:Parameter>
                <asp:Parameter
                  Name="RegisteredLevel"></asp:Parameter>
                <asp:Parameter
                  Name="ActiveLockVersion"></asp:Parameter>
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Type="String" 
                  Name="LicenseType"></asp:Parameter>
                <asp:Parameter Type="String" 
                  Name="UserName"></asp:Parameter>
                <asp:Parameter Type="String" 
                  Name="ApplicationName"></asp:Parameter>
                <asp:Parameter Type="Int32" 
                  Name="MaxLicenses"></asp:Parameter>
                <asp:Parameter Type="Int32" 
                  Name="ID"></asp:Parameter>
                <asp:Parameter Type="DateTime" 
                  Name="TimeLockedTo"></asp:Parameter>
                <asp:Parameter Type="Int32" 
                  Name="PeriodDays"></asp:Parameter>
                <asp:Parameter Type="String" 
                  Name="RegisteredLevel"></asp:Parameter>
                <asp:Parameter
                  Name="ActiveLockVersion"></asp:Parameter>
            </UpdateParameters>
            
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="UserName" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                            SelectCommand="SELECT [UserName] FROM [vw_aspnet_Users]">
                        </asp:SqlDataSource>
    </div>
    
</asp:Content>
